package com.nsaa.helper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nsaa.model.RoverMapper;
import com.nsaa.model.Rover;
import com.nsaa.service.PlateauService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class JsonReader {
    private static final String SPACE = " ";

    public static PlateauService readFromJson(String inputPath) {
        PlateauService plateauService = new PlateauService();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            InputStream input = new FileInputStream(inputPath);
            RoverMapper roverMapper = objectMapper.readValue(input, new TypeReference<RoverMapper>() {
            });
            String[] coordinate = roverMapper.getRightUpperCoordinate().split(SPACE);
            int x = Integer.parseInt(coordinate[0]);
            int y = Integer.parseInt(coordinate[1]);

            plateauService.setUpperRightX(x);
            plateauService.setUpperRightY(y);

            List<Rover> roverList = new ArrayList<>();
            roverMapper.getRovers().forEach(rvr -> {
                String[] initPoints = rvr.getCoordinate().split(SPACE);
                roverList.add(new Rover(Integer.parseInt(initPoints[0]),
                        Integer.parseInt(initPoints[1]), initPoints[2].charAt(0), rvr.getRoute()));
            });

            plateauService.setRoverList(roverList);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return plateauService;
    }
}
