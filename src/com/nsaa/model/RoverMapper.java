package com.nsaa.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Used to map deployed rovers json details in to POJO class
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoverMapper {
    @JsonProperty("rightUpperCoordinate")
    private String rightUpperCoordinate;

    @JsonProperty("rovers")
    private List<RoversSquard> rovers;

    public String getRightUpperCoordinate() {
        return rightUpperCoordinate;
    }

    public void setRightUpperCoordinate(String rightUpperCoordinate) {
        this.rightUpperCoordinate = rightUpperCoordinate;
    }

    public List<RoversSquard> getRovers() {
        return rovers;
    }

    public void setRovers(List<RoversSquard> rovers) {
        this.rovers = rovers;
    }
}