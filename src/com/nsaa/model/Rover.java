package com.nsaa.model;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Rover {
    /**
     * X coordinate
     */
    int x;
    /**
     * Y coordinate
     */
    int y;
    /**
     * Facing direction
     */
    char head;
    /**
     * Rover path
     */
    String path;

    /**
     * @param xAxis     - coordinate of rover's X axis
     * @param yAxis     - coordinate of rover's Y axis
     * @param headPoint - facing direction of rover's head
     * @param path      - moving path of a rover
     */
    public Rover(int xAxis, int yAxis, char headPoint, String path) {
        this.x = xAxis;
        this.y = yAxis;
        this.head = headPoint;
        this.path = path;
    }

    /**
     * @return rover's x coordinate
     */
    public int getX() {
        return x;
    }

    /**
     * @param x - rovers's x coordinate
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return rover's y coordinate
     */
    public int getY() {
        return y;
    }

    /**
     * @param - rovers's x coordinate
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @return rover's facing direction
     */
    public char getHead() {
        return head;
    }

    /**
     * @param head - rovers's facing direction
     */
    public void setHead(char head) {
        this.head = head;
    }

    /**
     * @return rover's moving path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path - rover's moving path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return rover's property in string
     */
    @Override
    public String toString() {
        return x + " " + y + " " + head;
    }
}
