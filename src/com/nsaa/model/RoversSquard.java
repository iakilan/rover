package com.nsaa.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Used to map deployed rovers initial point,
 * coordinate fromjson details in to POJO class
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoversSquard {
    @JsonProperty("coordinate")
    private String coordinate;

    @JsonProperty("route")
    private String route;

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }
}
