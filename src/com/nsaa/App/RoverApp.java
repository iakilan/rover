package com.nsaa.App;

import com.nsaa.helper.JsonReader;
import com.nsaa.service.PlateauService;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class RoverApp {
    private static final String PROP_PATH = "resource/config.properties";
    private static String inputJsonPath;
    private static String outputTextPath;

    public static void main(String[] args) throws IOException {

        FileReader reader = new FileReader(PROP_PATH);
        Properties prop = new Properties();
        prop.load(reader);
        inputJsonPath = prop.getProperty("inputFilePath");
        outputTextPath = prop.getProperty("outputFilePath");

        PlateauService plateauService = JsonReader.readFromJson(inputJsonPath);
        plateauService.moveRover();
        plateauService.writeToTxt(outputTextPath);
    }


}
