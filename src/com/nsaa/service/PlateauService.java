package com.nsaa.service;

import com.nsaa.model.Rover;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Service class for rovers navigation
 */
public class PlateauService {
    private final char NORTH = 'N';
    private final char EAST = 'E';
    private final char SOUTH = 'S';
    private final char WEST = 'W';

    private boolean isOutOfRange = false;
    private int upperRightX;
    private int upperRightY;
    private List<Rover> roverList;

    public void setUpperRightX(int upperRightX) {
        this.upperRightX = upperRightX;
    }

    public void setUpperRightY(int upperRightY) {
        this.upperRightY = upperRightY;
    }

    public List<Rover> getRoverList() {
        return roverList;
    }

    public void setRoverList(List<Rover> roverList) {
        this.roverList = roverList;
    }

    /**
     * Process rovers facing direction and move
     */
    public void moveRover() {
        this.roverList.stream().forEach(rover -> {
            if (!isOutOfRange) {
                for (char path : rover.getPath().toCharArray()) {
                    switch (path) {
                        /**
                         * Rotate facing direction clockwise
                         */
                        case 'L':
                            if (rover.getHead() == NORTH) {
                                rover.setHead(WEST);
                            } else if (rover.getHead() == EAST) {
                                rover.setHead(NORTH);
                            } else if (rover.getHead() == SOUTH) {
                                rover.setHead(EAST);
                            } else if (rover.getHead() == WEST) {
                                rover.setHead(SOUTH);
                            }
                            break;
                        /**
                         * Rotate facing direction anti clock wise
                         */
                        case 'R':
                            if (rover.getHead() == NORTH) {
                                rover.setHead(EAST);
                            } else if (rover.getHead() == EAST) {
                                rover.setHead(SOUTH);
                            } else if (rover.getHead() == SOUTH) {
                                rover.setHead(WEST);
                            } else if (rover.getHead() == WEST) {
                                rover.setHead(NORTH);
                            }
                            break;

                        case 'M':
                            if (rover.getHead() == NORTH) {
                                if (rover.getY() + 1 <= upperRightY) {
                                    rover.setY(rover.getY() + 1);
                                } else {
                                    isOutOfRange = true;
                                }
                            } else if (rover.getHead() == EAST) {
                                if (rover.getX() + 1 <= upperRightX) {
                                    rover.setX(rover.getX() + 1);
                                } else {
                                    isOutOfRange = true;
                                }
                            } else if (rover.getHead() == SOUTH) {
                                if (rover.getY() - 1 >= 0) {
                                    rover.setY(rover.getY() - 1);
                                } else {
                                    isOutOfRange = true;
                                }
                            } else if (rover.getHead() == WEST) {
                                if (rover.getX() - 1 >= 0) {
                                    rover.setX(rover.getX() - 1);
                                } else {
                                    isOutOfRange = true;
                                }
                            }
                            break;

                        default:
                            System.out.println("Invalid input");
                            break;
                    }
                }
            }
        });
    }

    /**
     * Gets Output path and writes the file as text.
     *
     * @param outputPath - A String representing path.
     */
    public void writeToTxt(String outputPath) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputPath))) {
            if (!isOutOfRange) {
                this.roverList.forEach(rover -> {
                    try {
                        writer.write(rover.toString() + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } else {
                writer.write("Out of plateau range, check the input data.");
            }
        }
    }
}